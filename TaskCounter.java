package org.sample;

public class TaskCounter {
    private int taskcount;

    public TaskCounter() {
        taskcount = 0;
    }
    public synchronized void inc() {
        taskcount++;
    }
    public synchronized void dec() {
        taskcount--;
    }
    public synchronized int getCount() {
        return taskcount;
    }
}
