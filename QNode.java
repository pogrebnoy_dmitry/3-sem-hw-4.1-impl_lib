package org.sample;

public class QNode<T> {
    private T value;
    private QNode<T> next;
    private QNode<T> previous;

    public QNode(T value) {
        this.value = value;
        next = null;
        previous = null;
    }

    public T getValue() {
        return value;
    }

    public void setNext(QNode<T> next) {
        this.next = next;
    }

    public void setPrev(QNode<T> previous) {
        this.previous = previous;
    }

    public QNode<T> getNext() {
        return next;
    }

    public QNode<T> getPrev() {
        return previous;
    }
}
