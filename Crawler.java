package org.sample;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Crawler implements Runnable {
    private ConcurrentListSet<String> pagesAlreadyVisited;
    private ThreadPool executor;
    private String url;
    private int curDepth;
    private final int MAX_DEPTH;
    private static final String USER_AGENT = "Chrome";
    private TaskCounter taskCounter;

    public Crawler(String url, TaskCounter taskCounter, ConcurrentListSet<String> pagesAlreadyVisited, ThreadPool executor, int curDepth, final int MAX_DEPTH) {
        this.url = url;
        this.taskCounter = taskCounter;
        this.pagesAlreadyVisited = pagesAlreadyVisited;
        this.executor = executor;
        this.curDepth = curDepth;
        this.MAX_DEPTH = MAX_DEPTH;
    }


    public void getAndParse() {
        try
        {
            Connection connection = Jsoup.connect(url).userAgent(USER_AGENT);
            Document htmlPage = connection.get();

            Elements linksOnPage = htmlPage.select("a[href]");
            //System.out.println("Take webpage - " + url);
            //System.out.println("Found (" + linksOnPage.size() + ") another links");
            for(Element link : linksOnPage)
            {
                /*
                Операция pagesAlreadyVisited.add(link.absUrl("href")); атомарна
                Добавляет элемент в ConcurrentSkipListSet и возвращает true, если в ConcurrentSkipListSet нет этого элемента,
                и не изменяет ConcurrentSkipListSet и возвращает false, если есть.

                Реализация add атомарна и соответствует поведению выше.

                Таким образом получается, что правильным решением будет написать if вот так:
                */
                if (link.absUrl("href") != null && link.absUrl("href") != "" && pagesAlreadyVisited.add(link.absUrl("href")) && curDepth <= MAX_DEPTH) {
                    taskCounter.inc();
                    executor.execute(new Crawler(link.absUrl("href"), taskCounter, pagesAlreadyVisited, executor, curDepth +1, MAX_DEPTH));
                }
            }
            taskCounter.inc();
            executor.execute(new PageSaver(htmlPage, url, taskCounter));
        }
        catch(IOException e)
        {
            //System.out.println("Webpage is broken :(  - " + url);
        }
    }

    public void run() {
        getAndParse();
        taskCounter.dec();
    }
}