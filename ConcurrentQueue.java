package org.sample;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConcurrentQueue<T> {
    private QNode<T> head;
    private QNode<T> tail;
    private Lock lock;

    public ConcurrentQueue() {
        head = null;
        lock = new ReentrantLock();
        tail = null;
    }

    public void push(T value) {
        QNode<T> newNode = new QNode<>(value);
        try {
            lock.lock();
            if (tail != null) {
                tail.setNext(newNode);
                newNode.setPrev(tail);
                tail = newNode;
            } else {
                head = newNode;
                tail = newNode;
            }
        }
        finally {
            lock.unlock();
        }
    }

    public T pop() {
        QNode<T> res = null;
        try {
            lock.lock();
            if (head != null) {
                res = head;
                head = head.getNext();
                if (head != null) {
                    head.setPrev(null);
                } else {
                    tail = null;
                }
            }
        }
        finally {
            lock.unlock();
        }
        if (res == null) {
            return null;
        } else {
            return res.getValue();
        }
    }
}