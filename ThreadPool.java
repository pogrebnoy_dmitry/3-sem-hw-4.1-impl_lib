package org.sample;

import java.util.concurrent.Executor;

public class ThreadPool implements Executor, AutoCloseable {
    private final ConcurrentQueue<Runnable> workQueue = new ConcurrentQueue<>();
    private volatile boolean isRunning = true;
    private Thread[] threads;

    public ThreadPool(int nThreads) {
        threads = new Thread[nThreads];
        for (int i = 0; i < nThreads; i++) {
            threads[i] = new Thread(new TaskWorker());
            threads[i].start();
        }
    }

    @Override
    public void execute(Runnable command) {
        if (isRunning) {
            workQueue.push(command);
        }
    }

    public void shutdown() {
        isRunning = false;
    }

    private final class TaskWorker implements Runnable {
        @Override
        public void run() {
            while (isRunning) {
                Runnable nextTask = workQueue.pop();
                if (nextTask != null) {
                    nextTask.run();
                }
            }
        }
    }

    @Override
    public void close() {
        shutdown();
    }
}
