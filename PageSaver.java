package org.sample;

import org.jsoup.nodes.Document;

import java.io.FileWriter;
import java.io.IOException;

public class PageSaver implements Runnable {
    private Document htmlPage;
    private String url;
    private TaskCounter taskcounter;

    public PageSaver(Document htmlPage, String url, TaskCounter taskcounter) {
        this.htmlPage = htmlPage;
        this.url = url;
        this.taskcounter = taskcounter;
    }

    public void run() {
        if (htmlPage == null) {
            taskcounter.dec();
            return;
        }
        String buffer = htmlPage.outerHtml();
        String name = "C:/Users/Дима/HW_4.1/trash/" + url.replaceAll(":", "_").replaceAll("/", "_").replaceAll("\\.", "_").replaceAll("\\?","_");
        //Название файла в Win не больше 260 символов
        if (name.length() > 250) {name = name.substring(0,250);}
        try (FileWriter saver = new FileWriter(name + ".html", false))
        {
            saver.write(buffer);
            //Чистим буфер IO
            saver.flush();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        finally {
            taskcounter.dec();
        }
    }
}