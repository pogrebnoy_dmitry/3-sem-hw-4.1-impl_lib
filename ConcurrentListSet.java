package org.sample;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConcurrentListSet<T> {
    private Node<T> head;
    private Node<T> tail;
    private Lock lock;

    public ConcurrentListSet() {
        this.head = null;
        this.tail = null;
        this.lock = new ReentrantLock();
    }

    public boolean add(T value) {
        Node<T> node = new Node<>(value);
        lock.lock();
        if (!contains(value)) {
            if (head == null) {
                head = node;
                tail = node;
            } else {
                Node tmp = tail;
                tmp.setNext(node);
                tail = node;
                node.setPrev(tmp);
            }
            lock.unlock();
            return true;
        } else {
            lock.unlock();
            return false;
        }
    }

    public boolean contains(T value) {
        if (head == null) {
        return false;
        }
        Node tmp = head;
        while (tmp.getNext() != null) {
            if (tmp.getValue() == value) {
                return true;
            }
            tmp = tmp.getNext();
        }
        return false;
    }
}